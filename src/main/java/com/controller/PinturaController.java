package com.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.entity.Pintura;
import com.repository.PinturaRepository;

@Controller
public class PinturaController {

	@Autowired
	private PinturaRepository pinturaRepository;

	@GetMapping("/")
	public String showViewPintura(Model model) {
		model.addAttribute("pinturas", this.pinturaRepository.findAll());
		return "index";
	}

	@GetMapping("/signuppintura")
	public String showSignUpForm(Pintura pintura) {
		return "add-pintura";
	}

	@PostMapping("/addpintura")
	public String addPintura(@Valid Pintura pintura, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "add-pintura";
		}

		this.pinturaRepository.save(pintura);
		model.addAttribute("pinturas", this.pinturaRepository.findAll());
		return "redirect:/";
	}

	// additional CRUD methods
	@GetMapping("/edit/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		Pintura pintura = this.pinturaRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid pintura Id:" + id));

		model.addAttribute("pintura", pintura);
		return "update-pintura";
	}

	@PostMapping("/update/{id}")
	public String updatePintura(@PathVariable("id") long id, @Valid Pintura pintura, BindingResult result,
			Model model) {
		if (result.hasErrors()) {
			pintura.setId(id);
			return "update-pintura";
		}

		this.pinturaRepository.save(pintura);
		model.addAttribute("pinturas", this.pinturaRepository.findAll());
		return "redirect:/";
	}

	@GetMapping("/delete/{id}")
	public String deletePintura(@PathVariable("id") long id, Model model) {
		Pintura pintura = this.pinturaRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid pintura Id:" + id));
		pinturaRepository.delete(pintura);
		model.addAttribute("pinturas", this.pinturaRepository.findAll());
		
		return "redirect:/";
	}

}
