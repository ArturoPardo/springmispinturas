package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Pintura;

@Repository
public interface PinturaRepository extends JpaRepository<Pintura, Long> {

}
