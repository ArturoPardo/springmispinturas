package com.service;

import com.entity.Pintura;

public interface PinturaService {

	Pintura findById(Long id);
	Pintura saveOrUpdate(Pintura parametrizacionPrograma);
}
