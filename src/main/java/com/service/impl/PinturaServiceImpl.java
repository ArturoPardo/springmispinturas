package com.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.entity.Pintura;
import com.repository.PinturaRepository;
import com.service.PinturaService;

import exception.NotFoundException;

@Service
@Transactional(readOnly = true)
public class PinturaServiceImpl implements PinturaService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PinturaServiceImpl.class);

	@Autowired
	PinturaRepository pinturaRepository;

	@Override
	public Pintura findById(final Long id) {
		LOGGER.debug("Getting Pintura with id {}", id);
		Optional<Pintura> pintura = pinturaRepository.findById(id);
		if (!pintura.isPresent()) {
			LOGGER.error("Pintura with id {} not found", id);
			throw new NotFoundException("Pintura con id " + id + " no encontrada");
		}
		return pintura.get();
	}

	@Override
	@Transactional(readOnly = false)
	public Pintura saveOrUpdate(Pintura pintura) {
		if (pintura.getId() != null) {
			LOGGER.debug("Pintura {} with id {}", pintura, pintura.getId());
			this.findById(pintura.getId());
		} else {
			LOGGER.debug("Saving new Pintura {}", pintura);
		}
		return this.pinturaRepository.save(pintura);
	}

}
